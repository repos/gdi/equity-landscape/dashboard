import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  linkActiveClass: 'router-link-exact-active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/engagement-domains',
      name: 'engagement-domains',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // pass an object to the component property to pass props to the component
      component: () => import('../views/EngagementDomainsView.vue')

    },
    {
      path: '/presence-and-growth',
      name: 'presence-and-growth',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // pass an object to the component property to pass props to the component
      component: () => import('../views/PresenceGrowthView.vue')

    },
    {
      path: '/affiliates-overview',
      name: 'affiliates-overview',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // pass an object to the component property to pass props to the component
      component: () => import('../views/AffiliatesView.vue')
    },
    {
      path: '/projects-and-languages',
      name: 'projects-and-languages',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/ProjectsLanguagesView.vue')
    }
  ]
})

export default router
