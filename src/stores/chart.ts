import { defineStore } from 'pinia';
import { useDataStore } from './data';
import { toTitleCase, compare, isEmpty } from '@/chartPrep';
import { DataUtils } from '@/charts/DataUtils';
import { TreeMapChart, type TreeMapArgs } from '@/charts/TreeMapChart';
import { ref } from 'vue';
import { BarChart } from '@/charts/BarChart';
import type { ResultType } from '@/charts/BarChart';
import type { Args } from '@/charts/BaseChart';
import { CardChart, type CardArgs } from '@/charts/CardChart';
import type { Geo } from '@/chartPrep';
import { convertDataToTableData} from '@/textualTablePrep';

const dataStore = useDataStore();


type MenuItem = {
    label: string;
    value: any;
}
type Country = {
    country_name: string;
    country_code_iso_3: string;
    country_code_iso_2: string;
}


export enum ChartType {
    WORLDMAP = 0,
    BARCHARTX = 1,
    BARCHARTY = 2,
    TREEMAP = 3,
    CARD = 4,
    TEXTUALTABLE = 5
}

export const useChartStore = defineStore("chart", () => {

    const state: Record<string, any> = {
        treeMapResults: ref<Array<Object> | null>(null),
        selection: ref(null),
        seriesResults: ref<ResultType | null>(null),
        cardResults: ref([]),
        geoList: ref(Array<string>()),

    };


    function _createMenuItems(data: Array<string>): Array<MenuItem> {

        let menuItems = [{ label: '', value: '' }];

        if (Array.isArray(data)) {
            menuItems = data.map((d) => {
                return {
                    label: toTitleCase(d),
                    value: d
                };
            });

            // sort the menu items
            menuItems.sort((a, b) => compare(a.label, b.label));
        }
        else {
            throw new Error(`Data: ${data} is not an array`);
        }

        return menuItems;
    }

    function getObservationMenuItems(dataType: number): Array<MenuItem> {
        const observations: Array<string> = dataStore.getObervations(dataType);
        if (!isEmpty(observations)) {
            return _createMenuItems(observations);
        }
        return [];

    }

    function getCountryMenuItems(): Array<MenuItem> {
        const countries: Array<Country> = dataStore.getStateVariable("countryData", []);
        if (!isEmpty(countries)) {
            return _createMenuItems(countries.map((c: Country) => c.country_name));
        }
        return [];

    }


    function createTreeMapObjects(dataType: number, dataLabel: string) {
        /**
         * This is for TreeMap data
         */


        const geoData: any = dataStore.getDataSets(dataType);

        if (isEmpty(geoData)) {
            console.error("No tree map data found");
            return;
        }

        const geoCountries = dataStore.getStateVariable("countryData", []);

        const geoMap = geoCountries.reduce((acc: any, item: any) => {
            const key = `${item.continent_name}_${item.subcontinent_name}`;

            if (!acc.has(key)) {
                acc.set(key, { continent_name: item.continent_name, subcontinent_name: item.subcontinent_name });
            }

            return acc;
        }, new Map());

        const geoArray = Array.from(geoMap.values());

        if (isEmpty(geoArray)) {
            console.error("No geo data found", geoArray);
            return;
        }


        const alternativeSrcField = "sub_continent";
        const srcField = "subcontinent_name";
        const targetField = "continent";
        const dataArray = geoData;
        const valueArray = geoArray;
        const alternativeTargetField = "continent_name";

        const mapDataArgs = {
            dataArray: dataArray,
            valueArray: valueArray,
            srcField: srcField,
            targetField: targetField,
            alternativeTargetField: alternativeTargetField,
            alternativeSrcField: alternativeSrcField,
        }

        const updatedData = DataUtils.updateFieldBasedOnAnother(mapDataArgs);


        const treeMapArgs: TreeMapArgs = {
            data: updatedData,
            label: dataLabel,
            measurementList: [],
            ignoreColumns: [],
            columnTypes: [],
            rootLevel: "global",
            levelColumn: "geography_level",
            itemKeys: {
                global: "global",
                continent: "continent",
                sub_continent: "sub_continent",
                country: "country_name",
            },
            levels: {
                global: { "root": true, "parent": null, "children": "continent" },
                continent: { "root": false, "parent": "global", "children": "sub_continent" },
                sub_continent: { "root": false, "parent": "continent", "children": "country" },
                country: { "root": false, "parent": "sub_continent", "children": null }
            },
            nameKey: "country_name",
        }

        const treeMapChart = new TreeMapChart(treeMapArgs);
        const treeMapData = treeMapChart.createObjects();
        state.treeMapResults.value = treeMapData;
    };

    function createSeriesObjects(dataType: number = 0, geoNameList: Array<string> = []) {
        /**
         * This is for Barcharts
         */


        const geos = dataStore.getDataSets(dataType);


        const barChart = new BarChart(geos);

        const measurementList = dataStore.getObervations(dataType);

        const args: Args = {
            measurementList: measurementList,
            label: "country_name",
            columnTypes: ["number"],
            ignoreColumns: ["year"],
            filterList: geoNameList
        }

        state.seriesResults.value = barChart.createObjects(args);
    };

    function createCardObjects(dataType: number = 0) {
        const data = dataStore.getFilteredGeos("global", dataType);
        const cardChart = new CardChart(data);

        const args: CardArgs = {
            measurementList: dataStore.getObervations(dataType),
            columnTypes: ["number"],
            ignoreColumns: ["year"],
            label: "",
            mapping: dataStore.getStateVariable("dataDefinitions", {})
        }

        const cardData = cardChart.createObjects(args);
        cardChart.updateTitle(cardData);

        state.cardResults.value = cardData;
    };

    function setGeoList(geography_level: string) {
        const geos = dataStore.getFilteredGeos(geography_level);
        state.geoList.value = geos.map((geo: Geo) => {
            return {
                label: geo.country_name,
                value: geo.country_name,
            };
        });

    }

    function getStateVariable<T>(key: string, default_return: T): T {
        if (key in state)
            return state[key].value;

        return default_return;
    }

    function setStateVariable(key: string, value: any) {
        state[key].value = value;
    }


    return {
        state,
        getObservationMenuItems,
        createTreeMapObjects,
        createSeriesObjects,
        createCardObjects,
        setGeoList,
        convertDataToTableData,
        isEmpty,
        getCountryMenuItems,
        getStateVariable,
        setStateVariable
    };

});