import { DataPoints } from '@/charts/DataPoints';
import { defineStore } from 'pinia'
import { ref } from 'vue';
import { observations, filterByColumn, compare, addToSessionStorage, getFromSessionStorage } from '@/chartPrep';
import type { Geo } from '@/chartPrep';
import { useAppStateStore } from '@/stores/appState';


import { ELDataFetcher } from '@/charts/ELDataFetcher';

// create enums for the different types of data
export enum Data {

    PRESENCE_AND_GROWTH = "presence_and_growth",
    ENGAGEMENT_DOMAINS = "engagement_domains",
    AFFILIATES = "affiliates",
}

export enum ObservationType {
    ENGAGEMENT_DOMAINS = 0,
    PRESENCE_AND_GROWTH = 1,
    AFFILIATES = 2
}

export type CountryData = {
    country_name: string,
    country_code_iso_2: string,
    country_code_iso_3: string,
    continent_name: string,
    subcontinent_name: string
}

export type MenuItem = {
    groupLabel: string,
    items: Array<{ value: string, label: string }>
}



const appStateStore = useAppStateStore();



/***
 * This store contains data that is used by multiple charts and should be loaded once and musn't be mutated.
 */

export const useDataStore = defineStore("data", () => {

    const dataPoints = new DataPoints(observations);



    const state: Record<string, any> = {
        presenceAndGrowth: ref(dataPoints.getDataPoint(Data.PRESENCE_AND_GROWTH)),
        engagementDomains: ref(dataPoints.getDataPoint(Data.ENGAGEMENT_DOMAINS)),
        affiliates: ref(dataPoints.getDataPoint(Data.AFFILIATES)),
        outputMetrics: ref(Array<any>()),
        treeMapData: ref<Record<string, Node> | null>(null),
        observationResults: ref<Array<Object> | null>(null),
        regionalMetrics: ref([]),
        worldData: ref(Array<any>()),
        countryData: ref([]),
        languageData: ref(Array<string>()),
        endangeredLanguages: ref(Array<string>()),
        briefProjectsEdited: ref(Array<string>()),
        dataDefinitions: ref(Array<string>()),
    };


    async function fetchAllData() {
        try {
            console.debug("Fetch World Data");

            const promises = [
                fetchWorldData(),
                fetchCountryData(),
                fetchRegionalMetrics(),
                fetchOutputMetrics(),
                fetchDataDefinitions(),
                fetchLanguageData(),
                fetchBriefProjectsEdited(),
                fetchEndangeredLanguages()
            ];

            await Promise.allSettled(promises);


        } catch (error) {
            console.error(error);
        }
    }


    async function fetchLanguageData() {
        const key = 'languageData';
        const label = 'languageData';
        const errorMessage = 'Error fetching language data';

        await fetchAndSetData(key, label, errorMessage);
    }

    async function fetchBriefProjectsEdited() {

        const key = 'briefProjectsEdited';
        const label = 'briefProjectsData';
        const errorMessage = 'Error fetching brief projects edited data';

        await fetchAndSetData(key, label, errorMessage);

    }

    async function fetchDataDefinitions() {
        const key = 'dataDefinitions';
        const label = 'dataDefinitions';
        const errorMessage = 'Error fetching data definitions';

        await fetchAndSetData(key, label, errorMessage);
    }


    async function fetchEndangeredLanguages() {

        const key = 'endangeredLanguages';
        const label = 'endangeredLanguagesData';
        const errorMessage = 'Error fetching endangered languages';

        await fetchAndSetData(key, label, errorMessage);
    }

    async function fetchOutputMetrics() {
        const key = 'outputMetrics';
        const label = 'outputData';
        const errorMessage = 'Error fetching output metrics';

        await fetchAndSetData(key, label, errorMessage);

    }

    async function fetchWorldData() {

        const key = 'worldData';
        const label = 'worldMapData';
        const errorMessage = 'Error fetching world data';

        await fetchAndSetData(key, label, errorMessage);

    }

    async function fetchCountryData() {
        const key = 'countryData';
        const label = 'countryData';
        const errorMessage = 'Error fetching country data';

        await fetchAndSetData(key, label, errorMessage);

    }

    async function fetchRegionalMetrics() {

        const key = 'regionalMetrics';
        const label = 'regionData';
        const errorMessage = 'Error fetching regional metrics';

        await fetchAndSetData(key, label, errorMessage);


    }

    async function fetchAndSetData(key: string, label: string, errorMessage: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const storageData = getFromSessionStorage(key);
            if (storageData) {
                console.debug('Invoking from local storage')
                state[key].value = storageData;
                resolve(storageData);
            } else {
                console.debug('Invoking from API')
                ELDataFetcher.fetchAndProcessData(label, errorMessage)
                    .then(data => {
                        state[key].value = data;
                        addToSessionStorage(key, data);
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error);
                    });
            }
        });
    }

    function getObervations(dataType: number) {
        let measurements: Array<string> = [];

        switch (dataType) {
            case ObservationType.ENGAGEMENT_DOMAINS:
                measurements = state.engagementDomains.value;
                break;
            case ObservationType.PRESENCE_AND_GROWTH:
                measurements = state.presenceAndGrowth.value;
                break;
            case ObservationType.AFFILIATES:
                measurements = state.affiliates.value;
                break;
        }

        return measurements;
    }

    function getObservationCardDescription(dataType: number): string {

        let description = "";

        switch (dataType) {
            case ObservationType.ENGAGEMENT_DOMAINS:
                description = `
            <p>
            <b>Overall Engagement</b> gives an estimate of the overall level of Wikimedia engagement in each country across the domains of readers, editors, and movement organizers in grants and affiliate spaces. The <b>Rank in Under-representation</b> can be used to understand the relative gap in Wikimedia presence and growth is compared to the population presence and growth in each country.
            </p>
            `;
                break;
            case ObservationType.PRESENCE_AND_GROWTH:
                description = `
            <p>
                 Each of the <b>domains are constructed using multiple facets</b>, based on which a domain metric is calculated. For example, Readership is is a combination of Reader Presence and Reader Growth. Usually there at least two facets for a domain, however, they need not always necessarily be presence and growth. For example, Affiliate Leadership comprises partly from Affiliate Presence and additional metrics. Please explore the schema for a detailed breakdown of the metrics.
            </p>
            `;
                break;

            case ObservationType.AFFILIATES:
                description = `

            <p>In acknowledgment of the diversity of groups contributing to the Wikimedia movement, the Wikimedia Foundation Board of Trustees recognizes models of affiliation within the Wikimedia movement – chapters, thematic organizations, and user groups.</p>

            <p>The data signals shared here are based on officially recognized affiliates active as of Dec 31st of the associated year of data.</p>

            <p>Importantly, there are movement organizing activities that are led by groups who are not recognized affiliates, these data do not represent those organizers.</p>
            `;
                break;
        }

        return description;

    }

    function getGeoDataString(dataType: number): string {
        let label: string = "";

        switch (dataType) {
            case ObservationType.ENGAGEMENT_DOMAINS:
            case ObservationType.PRESENCE_AND_GROWTH:
                label = "outputMetrics";
                break;
            case ObservationType.AFFILIATES:
                label = "regionalMetrics";
                break;
        }

        return label;
    }

    function getDataFromLabel(dataType: number) {
        const label = getGeoDataString(dataType);

        return (label && label !== "") ? state[label].value : [];

    }

    function filterByYear(data: Array<any>, year: number) {
        return filterByColumn(data, "year", year);
    }

    function getDataSets(dataType: number, filterYear: boolean = true) {
        const geos = getDataFromLabel(dataType);
        if (geos) {
            if (filterYear)
                return filterByYear(geos, appStateStore.currentYear);
        }

        return geos;
    }

    function isObserved(observation: string): boolean {
        return dataPoints.existsInDataPoints(observation);
    }

    function getFilteredGeos(geographyLevel: string, dataType: number = 0): Array<Geo> {

        let results: Array<Geo> = getAllGeos(dataType);
        if (results)
            results = results.filter((geo: Geo) => geo.geography_level === geographyLevel)

        return results;
    }

    function getAllGeos(dataType: number = 0): Array<Geo> {
        const datasets = getDataSets(dataType);

        let results: Array<Geo> = [];

        if (datasets)
            results = datasets.sort((a: Geo, b: Geo) => compare(a.country_name, b.country_name));

        return results;
    }

    /***
     * Returns a state variable if it exists, otherwise returns the default value passed in
     *
     * This is for TypeScript to be able to infer the type of the variable and not have to cast it
     *
     * @param key
     * @param default_return
     * @returns T
     */

    function getStateVariable<T>(key: string, default_return: T): T {
        if (key in state)
            return state[key].value;

        return default_return;
    }

    function createLookup() {
        return {
            findSubcontinent: function (countryName: string) {
                const country = state.countryData.value.find((c: CountryData) => c.country_name === countryName);
                return country ? country.subcontinent_name : null;
            },
            findContinent: function (subcontinentName: string) {
                const country = state.countryData.value.find((c: CountryData) => c.subcontinent_name === subcontinentName);
                return country ? country.continent_name : null;
            },
            findAllCountriesInSubcontinent: function (subcontinentName: string): Array<string> {
                const countries = state.countryData.value.filter((c: CountryData) => c.subcontinent_name === subcontinentName);
                // return countries as a list of country names
                return countries.reduce((acc: string[], c: CountryData) => {
                    if (!acc.includes(c.country_name)) {
                        acc.push(c.country_name);
                    }
                    return acc;
                }, []);
            },
            findAllSubcontinentsInContinent: function (continentName: string): Array<string> {
                const subcontinents = state.countryData.value.filter((c: CountryData) => c.continent_name === continentName);
                return subcontinents.reduce((acc: string[], c: CountryData) => {
                    if (!acc.includes(c.subcontinent_name)) {
                        acc.push(c.subcontinent_name);
                    }
                    return acc;
                }, []);
            }
        };
    }

    function getCountryData() {
        return state.countryData.value;
    }


    return {
        getObservationCardDescription,
        getObervations,
        getDataFromLabel,
        getDataSets,
        isObserved,
        getFilteredGeos,
        getStateVariable,
        fetchAllData,
        filterByYear,
        createLookup,
        getCountryData,
        getAllGeos
    };

});