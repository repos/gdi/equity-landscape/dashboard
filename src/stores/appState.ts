import { defineStore } from 'pinia';
import { ref } from 'vue';
import { yearChoice, geographyLevels } from '@/chartPrep';



export const useAppStateStore = defineStore("appState", () => {
    const state = {
            currentYear: ref(yearChoice[yearChoice.length - 1].value),
            years: yearChoice,
            currentCountry: ref('Albania'),
            currentGeographyLevel: ref('country'),
            geographyLevels: geographyLevels,
            multiSelectModelValue: ref(['']),
    }

    return {
        ...state,
    }
});