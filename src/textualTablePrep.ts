import { toTitleCase } from "./chartPrep";

// A map of column names to their display names
const columnMap: Record<string, string> = {
    "continent": "Continent",
    "sub_continent": "Subcontinent",
    "country_name": "Country"
};

// Parameters for converting data to table data
interface ConvertDataToTableDataParams {
    data: Array<any>, // The data to convert to table data
    columns?: Array<string>, // The columns to include in the table
    measurements?: Array<string>, // The measurements to include in the table
    hideColumns?: Array<string>, // The columns to hide in the table
    columnOrder?: Array<string>, // The order to display the columns in the table
    rowOrder?: Array<string>, // The order to display the rows in the table
    rowOrderDirection?: string // The direction to sort the rows in the table
}

// Returns a table item object
function _getTableItem(item: any, hide: boolean = false) {
    return {
        label: toTitleCase(item),
        field: item,
        hidden: hide,
        formatFn: (item: number) => (item === 0 || item === null ? '0' : item)
    }
}

// Handles the column map
function _handleColumnMap(key: string, array: Array<any>) {
    if (key in columnMap) {
        array.push({ label: columnMap[key], field: key });
    }
}

// Returns the columns for the table
function _getColumns(row: any, measurements?: Array<string>, hideColumns?: Array<string>) {
    const columns: Array<any> = [];

    if (!row) { return columns; }

    for (const key in columnMap) {
        if (hideColumns?.includes(key)) { continue; }
        _handleColumnMap(key, columns);
    }

    for (const [key, _] of Object.entries(row)) {
        if (key === 'year' || key in columnMap || (hideColumns?.includes(key))) { continue; }
        if (shouldIncludeColumn(key, measurements)) {
            columns.push(_getTableItem(key, hideColumns?.includes(key)));
        }
    }

    return columns;
}

// Returns the rows for the table
function _getRows(data: any, measurements?: Array<string>) {
    if (!data) { return []; }

    const rows = data.map((row: any) => {
        const tableRow: any = {};

        for (const [key, value] of Object.entries(row)) {
            if (key === 'year') continue;

            if (shouldIncludeColumn(key, measurements)) {
                tableRow[key] = value;
            }
        }

        return tableRow;
    });

    sortRows(rows);

    return rows;
}

function shouldIncludeColumn(key: string, measurements?: Array<string>) {
    return !measurements || measurements.includes(key) || key in columnMap;
}

function sortRows(rows: Array<any>) {
    const sortKeys = ['continent', 'sub_continent', 'country_name'];
    rows.sort((a: any, b: any) => {
        for (const key of sortKeys) {
            if (a[key] < b[key]) return -1;
            if (a[key] > b[key]) return 1;
        }
        return 0;
    });
}

// Sorts the columns in the table
function _sortColumns(columns: Array<any>, order: Array<string>) {
    columns.sort((a, b) => {
        const aIndex = order.indexOf(a.field);
        const bIndex = order.indexOf(b.field);

        if (aIndex === -1) {
            return 1;
        }

        if (bIndex === -1) {
            return -1;
        }

        return aIndex - bIndex;
    });

    return columns;
}

// Sorts the rows in the table
function _sortRowsByColumns(rows: Array<any>, columns: Array<string>, direction: string = 'asc') {
    rows.sort((a: any, b: any) => {
        let result = 0;
        for (const column of columns) {
            if (a[column] < b[column]) {
                result = -1;
                break;
            }
            if (a[column] > b[column]) {
                result = 1;
                break;
            }
        }
        return direction === 'asc' ? result : -result;
    });

    return rows;
}

// Converts data to table data
function convertDataToTableData({ data, measurements, hideColumns, columnOrder, rowOrder, rowOrderDirection }: ConvertDataToTableDataParams) {

    const columns = _getColumns(data[0], measurements, hideColumns);
    const rows = _getRows(data, measurements);

    const sortedColumns = columnOrder ? _sortColumns(columns, columnOrder) : columns;

    const sortedRows = rowOrder ? _sortRowsByColumns(rows, rowOrder, rowOrderDirection) : rows;

    return { 'rows': sortedRows, 'columns': sortedColumns };
}

// Export the necessary functions and types
export { convertDataToTableData }
export type { ConvertDataToTableDataParams }