import { DataFetcher } from '@/charts/DataFetcher';
import { URL } from '@/constants';
import { DataUtils } from './DataUtils';

/**
 * This class is responsible for fetching Equity Landscape data from the backend.
 */
class ELDataFetcher extends DataFetcher {
    private static data: Record<string, Array<any>> = {
        outputData: [],
        worldMapData: [],
        countryData: [],
        regionData: [],
        briefProjectsData: [],
        endangeredLanguagesData: [],
        languageData: [],
        dataDefinitions: [],
    };

    private static urlMap: Record<string, string> = {
        outputData: URL.OUTPUT_METRICS_URL,
        worldMapData: URL.WORLD_DATA_URL,
        countryData: URL.COUNTY_DATA_URL,
        regionData: URL.REGIONAL_METRICS_URLS,
        briefProjectsData: URL.BRIEF_PROJECTS_EDITED_URL,
        endangeredLanguagesData: URL.UNESCO_ENDANGERED_LANGUAGES_URL,
        languageData: URL.LANGUAGE_DATA_URL,
        dataDefinitions: URL.DATA_DEFINITION_URL,
    };

    private static async fetchDataIfEmpty(label: string) {
        if (!(label in this.data)) {
            throw new Error(`Key ${label} not found in data`);
        }

        if (DataUtils.isEmpty(this.data[label])) {
            this.data[label] = await this.fetchData(this.urlMap[label]);
        }

        return this.data[label];
    }


    public static async fetchAndProcessData(label: string, errorMessage: string): Promise<Array<any>> {
    let results: Array<any> = [];
    try {
        results = await this.fetchDataIfEmpty(label);

        if (results.length === 0) {
        return []; // no data loaded
        }

        if (!Array.isArray(results)) {
           return results;
        }

        results.forEach((result) => {
        Object.keys(result).forEach((key) => {
            if (typeof result[key] === 'number') {
            result[key] = Math.round(result[key]);
            }
        });
        });
    } catch (error) {
        console.error(error, errorMessage);
    }

    return results;
    }

}

export { ELDataFetcher };
