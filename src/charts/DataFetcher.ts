class DataFetcher {
    constructor() {
        throw new Error("DataFetcher is a static class and cannot be instantiated");
    }
    protected static async fetchData(url: string) {
        const response = await fetch(url);
        return response.json();
    }
}

export { DataFetcher }