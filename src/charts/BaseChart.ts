import { compare } from '@/chartPrep'
interface Args {
    measurementList: Array<string>;
    label: string;
    ignoreColumns: Array<string>;
    columnTypes: Array<string>;
    filterList?: Array<string>;
    data?: Array<any>;
    nameKey?: string;
}

class BaseChart {

    protected data: Array<any>;

    constructor(data: Array<any>) {

        if (new.target === BaseChart) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }

        this.data = data;
    }

    public createObjects(args: Args): any | void {
        throw new Error('Method not implemented.');
    }

    public filterByList(colummName: string, filterList: Array<string>) {
        return this.data.filter((d) => filterList.includes(d[colummName])).sort((a, b) => compare(a[colummName], b[colummName]));
    }

    public filterByColumn(column: string, value: any) {
        return this.data.filter((d) => d[column] === value).sort((a, b) => compare(a[column], b[column]));
    }

    protected getColumnNames(args: Args): Set<string> {
        const firstObject = this.data[0];
        const columnNames: Set<string> = new Set();

        // Create column names
        for (const key in firstObject) {
            const value = firstObject[key];
            if (this.isValidItem(args, value, key)) {
                columnNames.add(key);
            }

        }

        return columnNames;
    }

    protected isValidItem(args: Args, value: any, item: string) {
        return args.columnTypes.includes(typeof value) && !args.ignoreColumns.includes(item) && args.measurementList.includes(item);
    }




}

export { BaseChart }
export type { Args }
