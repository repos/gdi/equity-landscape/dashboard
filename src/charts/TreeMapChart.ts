import { BaseChart, type Args } from "./BaseChart";

type LevelData = {
    [key: string]: {
        root: boolean;
        parent: string | null;
        children: string | null;
    };
};

type NodeData = {
    [key: string]: any;

}

type ConvertedNode = {
    value: number;
    name: string;
    path: string;
    children: ConvertedNode[];
};

interface TreeMapArgs extends Args {
    rootLevel: string;
    itemKeys: Record<string, string>;
    levelColumn: string;
    levels: LevelData;
    nameKey: string;
}



/**
 * Class representing a TreeMapChart.
 * @class TreeMapChart
 */
class TreeMapChart extends BaseChart {


    private args: TreeMapArgs;

    constructor(treeMapArgs: TreeMapArgs) {
        if (!treeMapArgs.data) {
            throw new Error("No data provided");
        }
        super(treeMapArgs.data);
        this.args = treeMapArgs;
    }

    private setKey(
        level: string,
        parentValue: string | undefined,
        item: NodeData,
    ): string | null {
        /**
         * Generate a key based on the level, parentValue, and item.
         *
         * This function generates a key to be used in the hierarchical structure
         * of the data. The key is determined based on the level and the parent
         * of the current item in the hierarchy.
         *
         * @param level The current level in the hierarchy.
         * @param parentValue The value of the parent item in the hierarchy.
         * @param item The current item (row) from the list of dictionaries.
         * @param levels An object mapping levels to their properties (root, parent, children).
         * @param itemKeys An object mapping levels to their corresponding item key names.
         *
         * @returns The generated key to be used in the hierarchical structure.
         *
         */

        let key: string | null = null;
        const levelData = this.args.levels[level];

        if (levelData.root || levelData.parent && parentValue === item[this.args.itemKeys[levelData.parent]]) {
            key = item[this.args.itemKeys[level]];
        }

        return key;
    }

    private buildHierarchy(
        data: NodeData[],
        level: string,
        parentValue?: string
    ): Record<string, NodeData> {
        /**
         * Recursively build the hierarchical structure based on the level and parentValue.
         *
         * This function constructs a hierarchical structure for the data. It takes a list of
         * dictionaries containing information, the current level, and an optional parent value.
         * It generates the appropriate key for each item and adds it to the hierarchy. If the
         * current level is not the last level in the levels object, the function is called
         * recursively with the next level in the hierarchy and the current key as the parent value.
         *
         * @param data A list of dictionaries containing data, where each dictionary represents
         *             a row in the original data.
         * @param level The current level in the hierarchy.
         * @param levels An object mapping levels to their properties (root, parent, children).
         * @param itemKeys An object mapping levels to their corresponding item key names.
         * @param parentValue The value of the parent item in the hierarchy.
         *
         * @returns A dictionary representing the hierarchical structure of the data for
         *          the given level and parent value.
         *
         */

        const hierarchy: Record<string, NodeData> = {};
        for (const item of data) {

            if (item[this.args.levelColumn] === level) {
                const key = this.setKey(level, parentValue, item);

                if (key !== null && !(key in hierarchy)) {
                    hierarchy[key] = item;

                    const childLevel = this.args.levels[level].children;
                    if (childLevel) {
                        item.children = this.buildHierarchy(data, childLevel, key);
                    }
                }
            }
        }

        return hierarchy;
    }

    /**
     * Create a hierarchical structure from the data.
     *
     * @returns The hierarchical structure of the data, or the original data if it is not defined.
     */
    private createGeoHierarchy(): Array<any> | Record<string, NodeData> {
        if (this.data) {
            const hierarchy = this.buildHierarchy(this.data, this.args.rootLevel);
            return hierarchy;
        }
        return this.data;
    }

    private isNode(input: any): input is NodeData {
        return (input && typeof input === 'object');
    }


    /**
     *
     * Process a node in the hierarchical structure.
     *
     * @param node node to process
     * @param path path to node
     * @param label label to use
     * @returns converted node
     */
    private processNode(node: NodeData, path: string, label: string): ConvertedNode {
        const children: ConvertedNode[] = [];

        if (node.children) {
            for (let child of Object.values(node.children)) {
                 if (this.isNode(child)) {
                    children.push(this.processNode(child, `${path}/${child[this.args.nameKey]}`, label));
                 }
            }
        }

        return {
            value: label in node ? node[label] as number : 0,
            name: node[this.args.nameKey],
            path: path,
            children
        };
    }

    /**
     * Convert the hierarchical structure to the new format.
     * @returns converted node
     */

    private convertToNewFormat(): ConvertedNode[] {
        const hierarchy = this.createGeoHierarchy();
        if (typeof hierarchy === "object") {
            const result: ConvertedNode[] = [];

            for (let [nodeName, node] of Object.entries(hierarchy)) {
                result.push(this.processNode(node, nodeName, this.args.label));
            }

            return result;
        }

        throw new Error("Hierarchy data not available or not an object.");
    }

    public createObjects(): ConvertedNode[] {
        return this.convertToNewFormat();

    }
}


export { TreeMapChart }
export type { TreeMapArgs, ConvertedNode }