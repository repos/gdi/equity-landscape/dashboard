interface MapDataArgs {
    dataArray: Array<any>;
    valueArray: Array<any>;
    srcField: string;
    targetField: string;
    alternativeTargetField?: string;
    alternativeSrcField?: string;
}

class DataUtils {

    private constructor() { }


    /**
     *
     * @param srcFieldValue source field value
     * @param srcField source field label
     * @param targetField target field label
     * @param valueArray array of data dictionaries
     * @returns string or null
     */
    private static getFieldValue(srcFieldValue: string, srcField: string, targetField: string, valueArray: Array<any>): string | null {
        const data = valueArray.find(item => item[srcField] === srcFieldValue);
        return data ? data[targetField] : null;
    }

    public static updateFieldBasedOnAnother(args: MapDataArgs): any[] {
        /**
         * Update a field in a list of data dictionaries based on another field.
         *
         * @param args An object of MapDataArgs to be used as arguments.
         *
         * @returns An array of data dictionaries with the updated field.
         */
        const actualSrcField = args.alternativeSrcField ?? args.srcField;
        const actualTargetField = args.alternativeTargetField ?? args.targetField;

        for (const dct of args.dataArray) {
            const srcFieldValue = dct[actualSrcField];
            if (srcFieldValue && !dct[args.targetField]) {
                const targetFieldValue = this.getFieldValue(srcFieldValue, args.srcField, actualTargetField, args.valueArray);
                dct[args.targetField] = targetFieldValue;
            }
        }

        return args.dataArray;
    }


    public static isEmpty(obj: any): boolean {
        return obj === null || obj === undefined || obj.length === 0;
    }

    public static getMinMax(data: Array<any>, field: string): { min: number, max: number } {
        /**
         * Retrieve the minimum and maximum values for a given field from a list of data.
         *
         * This function searches a list of data dictionaries for the minimum and maximum
         * values for a given field and returns the corresponding values. If no match is
         * found, the function returns null.
         *
         * @param data The data for which the minimum and maximum values are to be retrieved.
         * @param field The name of the field to match.
         *
         * @returns An array containing the minimum and maximum values for the given field.
         *
         */

        const min = Math.min(...data.map((d) => d[field]));
        const max = Math.max(...data.map((d) => d[field]));

        return { min, max }

    }
}

export { DataUtils, type MapDataArgs }