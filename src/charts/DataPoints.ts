type DataPoint = {
    [key: string]: Array<string>;
}


class DataPoints {

    private dataPoints: DataPoint;

    constructor(dataPoints: DataPoint) {
        this.dataPoints = dataPoints;
    }


    public getDataPoints() {
        return this.dataPoints;
    }

    public getDataPoint(key: string) {
        return this.dataPoints[key];
    }

    public existsDataPoint(key: string, value: string) {
        return this.dataPoints[key].includes(value);
    }

    public existsInDataPoints(value: string): boolean {
        for (const key in this.dataPoints) {
            if (this.dataPoints[key].includes(value)) {
                return true;
          }
        }
        return false;
    }

}

export { DataPoints };