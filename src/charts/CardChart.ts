import { BaseChart } from "./BaseChart";
import type { Args } from "./BaseChart";
import { toTitleCase } from "@/chartPrep";

interface CardArgs extends Args {
    mapping: Record<string, string>;
}


/**
 * Represents a CardChart with methods to create and manipulate the chart.
 * @class CardChart
 */
class CardChart extends BaseChart {

    constructor(data: Array<any>) {
        super(data);
    }

    /**
     * Creates an array of series objects using the provided arguments.
     * @param args - The arguments to create series objects.
     * @returns An array of objects containing the title and description.
     */
    public createObjects(args: CardArgs): any {
        const firstObject = this.data[0];
        const measurementObjects: Array<any> = [];

        for (const key in firstObject) {
            const value = firstObject[key];
            if (this.isValidItem(args, value, key)) {
                measurementObjects.push({
                    title: key,
                    description: value,
                    tooltip: args.mapping ? args.mapping[key] : toTitleCase(key),
                });
            }
        }


        return measurementObjects;
    }


    /**
     * Updates the title and description of each card.
     * @param data - The data to update.
     * @param decimalFormat - The number of decimal places to format the description.
     */
    public updateTitle(data: Array<any>) {
        data.forEach((card) => {
            card.title = toTitleCase(card.title);
            card.description = Math.round(card.description);
        });
    }
}

export { CardChart }
export type { CardArgs }