import { BaseChart } from './BaseChart';
import type { Args } from './BaseChart';


type Measurements = Array<string>;

interface SeriesObject {
    name: string;
    type: string;
    emphasis?: {
        focus: string;
    };
    data: number[];
}

type ResultType = [Array<string>, Array<SeriesObject>];

type SeriesObjects = Array<SeriesObject>;

interface SeriesArgs {
    measurementList: Measurements;
    label: string;
    types: Array<string>;
    ignoreColumns: Array<string>;
    filterList: Array<any>;
}

/**
 * Represents a BarChart with methods to create and manipulate the chart.
 * @class BarChart
 */
class BarChart extends BaseChart{

    constructor(data: Array<any>) {
        super(data);
    }

    /**
     * Creates an array of series objects using the provided arguments.
     * @param Args - The arguments to create series objects.
     * @returns A tuple containing chart data and series objects.
     */
    public createObjects(seriesArgs: Args): ResultType  {
        if (seriesArgs.measurementList.length === 0) {
            throw new Error('No measurement list provided.');
        }

        let seriesObjects: SeriesObjects = [];
        const chartData: Array<any> = [];
        const measurementSet = this.getColumnNames(seriesArgs);

        if (seriesArgs.filterList && seriesArgs.filterList.length > 0) {
        const filteredData: Array<any> = this.filterByList(seriesArgs.label, seriesArgs.filterList);

        filteredData.forEach((element: any) => {
            const dataObj = element[seriesArgs.label];
            chartData.push(dataObj);

            measurementSet.forEach((measurement) => {
                const value = element[measurement];
                seriesObjects = this.updateSeriesObjects(seriesObjects, measurement, value);
            });
        });
    }

        return [chartData, seriesObjects];
    };

    /**
     * Updates or adds a new series object with the given key and value.
     * @param seriesObjects - The current series objects.
     * @param key - The key to use.
     * @param value - The value to use.
     * @returns The updated series objects.
     */
    private updateSeriesObjects = (seriesObjects: Array<SeriesObject>, key: string, value: number) => {
        const series = seriesObjects.find((s) => s.name === key);

        if (series) {
            series.data.push(value);
        } else {
            seriesObjects.push({
                name: key,
                type: 'bar',
                emphasis: {
                    focus: 'series',
                },
                data: [value],
            });
        }

        return seriesObjects;
    };

    /**
     * Updates the data of the chart.
     * @param data - The new data.
     */
    updateSeriesData(data: Array<any>) {
        this.data = data;
    }
}


export { BarChart };
export type { SeriesObject, SeriesArgs, ResultType, Measurements, SeriesObjects };