import { createApp } from 'vue';
import { createPinia } from 'pinia';
import Header from '@/components/Header.vue';
import Footer from '@/components/Footer.vue';
import VueGoodTablePlugin from 'vue-good-table-next';


// import the styles
import 'vue-good-table-next/dist/vue-good-table-next.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome, faFlag, faChartLine, faLanguage, faAtom, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faHome, faFlag, faChartLine, faAtom, faLanguage, faTimes)

import App from './App.vue'
import router from './router'


import '@wikimedia/codex/dist/codex.style.css'

const app = createApp(App)

app.use(createPinia());
app.use(router);
app.component('Header', Header);
app.component('Footer', Footer);
app.component('font-awesome-icon', FontAwesomeIcon);
app.use(VueGoodTablePlugin);
app.mount('#app');

