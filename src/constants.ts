const BASE_URL = "http://localhost:8000/";

const URL = {
    'BASE_URL': BASE_URL,
    'OUTPUT_METRICS_URL': BASE_URL + "metrics/output_metrics/output_metrics.json",
    'HIERARCHICAL_METRICS_URL': BASE_URL + "metrics/hierarchical_metrics/geo_hierarchy.json",
    'WORLD_DATA_URL': BASE_URL + "world_data/world_data.json",
    'REGIONAL_METRICS_URLS': BASE_URL + "metrics/regional_metrics/regional_metrics.json",
    'BRIEF_PROJECTS_EDITED_URL': BASE_URL + "metrics/language_data/brief_projects_edited.json",
    'LANGUAGE_DATA_URL': BASE_URL + "metrics/language_data/language_data.json",
    'UNESCO_ENDANGERED_LANGUAGES_URL': BASE_URL + "metrics/language_data/unesco_endangered_languages.json",
    'COUNTY_DATA_URL': BASE_URL + "world_data/country_data.json",
    "DATA_DEFINITION_URL": BASE_URL + "definitions/data_definitions.json",
};



export { URL };