/**
 * This file contains utility functions and types used for chart preparation.
 *
 * Types:
 * - ObservationType: an object type that maps strings to arrays of strings.
 * - Geo: an object type that maps strings to any type.
 *
 * Functions:
 * - toTitleCase(str: string): converts a string to title case.
 * - addToSessionStorage(key: string, value: any): adds an item to session storage with a 30 minute expiration.
 * - getFromSessionStorage(key: string): retrieves an item from session storage if it exists and is not expired.
 * - compare(a: any, b: any): compares two values and returns -1, 0, or 1 depending on their order.
 * - levenshteinDistance(a: string, b: string): calculates the Levenshtein distance between two strings.
 * - findSimilarValue(value: string, list: Array<string>, maxDistancePercentage: number): finds the most similar value in a list to a given value.
 * - filterByColumn(data: Array<any>, column: string, value: any): filters an array of objects by a column value.
 * - filterData(data: Array<any>, columnName: string, filterList: Array<string>): filters an array of objects by a list of values in a specific column.
 * - isEmpty(obj: any): checks if an object is null, undefined, or has a length of 0.
 *
 * Constants:
 * - observations: an ObservationType object that maps strings to arrays of strings.
 * - geographyLevels: an array of objects with label and value properties representing different levels of geography.
 * - yearChoice: an array of objects with label and value properties representing different years.
 */

type ObservationType = {
  [key: string]: Array<string>;
};

type Geo = {
  [key: string]: any;
};

function toTitleCase(str: string) {
  return str.replace(/_/g, ' ').toLowerCase().replace(/(?:^|\s|-)\w/g, function (match) {
    return match.toUpperCase();
  });
}

function addToSessionStorage(key: string, value: any) {
  // add a 30 minute expiration to the session storage
  const now = new Date();
  const item = {
    value: value,
    expiry: now.getTime() + 1800000
  };
  if (!localStorage.getItem(key)) {
    sessionStorage.setItem(key, JSON.stringify(item));
  }
}

function getFromSessionStorage(key: string): string | null {
  // check if the item is in the session storage
  const itemStr = sessionStorage.getItem(key);

  if (!itemStr) {
    return null;
  }

  const item = JSON.parse(itemStr);
  const now = new Date();
  if (now.getTime() > item.expiry) {
    // If the item is expired, delete the item from storage
    // and return null
    sessionStorage.removeItem(key);
    return null;
  }
  return item.value;
}

const observations: ObservationType = {
  "engagement_domains": [
    "overall_engagement",
    "readership",
    "editorship",
    "grants_leadership",
    "affiliates_leadership",
    "population",
    "population_underrepresentation",
    "programs_leadership",
    "overall_enablement",
    "access",
    "freedom"
  ],
  "presence_and_growth": [
    "reader_presence",
    "reader_growth",
    "editor_presence",
    "editor_growth",
    "grants_presence",
    "grants_growth",
    "affiliate_presence",
    "affiliate_growth",
    "access_presence",
    "access_growth",
    "freedom_presence",
    "freedom_growth"
  ],
  "affiliates": [
    "affiliate_size_max",
    "count_operating_affiliates",
    "affiliate_tenure_max"
  ]
};


const geographyLevels = [{ label: "Country", value: "country" }, { label: "Subcontinent", value: "sub_continent" }, { label: "Continent", value: "continent" }];
const yearChoice = [{ label: '2021', value: 2021 }, { label: '2022', value: 2022 }].sort((a, b) => compare(a.value, b.value));

function compare(a: any, b: any) {
  if (a < b) {
    return -1;
  }
  if (a > b) {
    return 1;
  }
  return 0;
}


// function to filter an array of objects by a column value
function filterByColumn(data: Array<any>, column: string, value: any) {
  return data.filter((d) => d[column] === value).sort((a, b) => compare(a[column], b[column]));
}

function filterData(data: Array<any>, columnName: string, filterList: Array<string>) {
  return data.filter((d) => filterList.includes(d[columnName])).sort((a, b) => compare(a[columnName], b[columnName]));
}

function isEmpty(obj: any) {
  return obj === null || obj === undefined || obj.length === 0;
}


export { observations, geographyLevels, yearChoice, compare,  toTitleCase, filterByColumn, filterData, isEmpty, addToSessionStorage, getFromSessionStorage}
export type { ObservationType, Geo };

